/*
 * (C) Copyright 2015 Konstantin Chigintsev (kchigintsev@gmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 */

#ifndef HTTP_TOY_HTTP_SERVER_HPP
#define HTTP_TOY_HTTP_SERVER_HPP

#include <string>
#include <vector>

#include <boost/asio.hpp>
#include <boost/function.hpp>
#include <boost/shared_ptr.hpp>

#include "http_connection.hpp"

class HTTPRequest;
class HTTPResponse;
template<typename MessageType>
class HTTPDataReader;

class HTTPServer
{
  struct ClientContext;

public:

  typedef boost::shared_ptr<HTTPConnection> ConnectionPtr;
  typedef boost::shared_ptr<HTTPRequest> RequestPtr;
  typedef boost::shared_ptr<HTTPResponse> ResponsePtr;

  HTTPServer(boost::asio::io_service& io_svc);


  typedef boost::function<ResponsePtr (boost::system::error_code&, HTTPRequest&)> RequestProcessor;

  void start_server(const std::string& protocol, unsigned int port, RequestProcessor processor);


  class ServerContext
  {
  public:
    ServerContext(boost::asio::io_service& io_svc,
        HTTPServer * srv, const std::string& protocol,
        unsigned int port,
        RequestProcessor processor);

    ~ServerContext();

    //template<typename SocketType>
    void on_accepted(const boost::system::error_code& error,
        HTTPServer::ConnectionPtr connection,
        boost::shared_ptr<HTTPServer::ServerContext> ctx);

    friend class HTTPServer;
    HTTPServer * srv;

    boost::asio::io_service& io_svc_;
    boost::shared_ptr< HTTPDataReader<HTTPRequest> > reader_;
    std::string protocol;
    unsigned int port;
    RequestProcessor processor;
  };

private:

  struct ClientContext
  {
    friend class HTTPServer;
    friend class ServerContext;

    boost::shared_ptr<ServerContext> ctx;

    ConnectionPtr connection;
    RequestPtr request;
    ResponsePtr response;
    std::vector<char> response_buf;

    //on_request_received
    void on_message_received(const boost::system::error_code& error,
        RequestPtr request,
        boost::shared_ptr<ClientContext> cln_ctx);

    void on_response_sent(const boost::system::error_code& error, size_t bytes ,
        boost::shared_ptr<ClientContext> cln_ctx );

    ~ClientContext()
    {
      std::cout << __FUNCTION__ << std::endl;
    }
  };

  //void start_server_impl(const std::string& protocol, unsigned int port, RequestProcessor processor);
  void start_server_impl(boost::shared_ptr<HTTPServer::ServerContext> srv_ctx);

  boost::asio::io_service& io_svc_;
  HTTPConnector acceptor_;
};

#endif // HTTP_TOY_HTTP_SERVER_HPP
