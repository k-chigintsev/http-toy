/*
 * (C) Copyright 2015 Konstantin Chigintsev (kchigintsev@gmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 */

#include <iostream>

#include "options.hpp"


int main(int argc, char* argv[])
{
  try
  {
    ClientOptions options(argc, argv);
    options.run();

    /*
    boost::system::error_code dummy;
    URL::parse_url(dummy, "http://localhost:80/some/path");
    URL::parse_url(dummy, "https://localhost:80/some/path");
    URL::parse_url(dummy, "http://localhost/some/path");
    URL::parse_url(dummy, "https://localhost/some/path");
    URL::parse_url(dummy, "localhost:80/some/path");
    URL::parse_url(dummy, "localhost:443/some/path");
    URL::parse_url(dummy, "localhost");
    URL::parse_url(dummy, "192.168.1.1");
    URL::parse_url(dummy, "http://www.yandex.ru/");
    return 0;
    */

    //boost::asio::io_service io_svc;
    //HTTPClient client(io_svc);
    //boost::shared_ptr<boost::asio::io_service::work> work( new boost::asio::io_service::work(io_svc) );
    //boost::thread worker( boost::bind(&boost::asio::io_service::run, &io_svc ) );
    //work.reset();
    //worker.join();
    std::cout << "done successfully!" << std::endl;
  }
  catch (std::exception& ex)
  {
    std::cerr << "Error occurred: " << ex.what() << std::endl;
  }
  return 0;
}
