/*
 * (C) Copyright 2015 Konstantin Chigintsev (kchigintsev@gmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 */

#ifndef HTTP_TOY_OPTIONS_HPP
#define HTTP_TOY_OPTIONS_HPP

#include <iostream>
#include <string>
#include <vector>
#include <boost/bind.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/make_shared.hpp>
#include <boost/program_options.hpp>
#include <boost/thread.hpp>


#include "http_client.hpp"
#include "http_request.hpp"
#include "http_response.hpp"
#include "url.hpp"

class ClientOptions
{
public:
  ClientOptions(int argc, char* argv[])
  : options_("Program options")
  {

    boost::program_options::positional_options_description url_option;
    url_option.add("url", -1);

    options_.add_options()
      ("url", boost::program_options::value<std::string>(&url_))
      ("query,q", boost::program_options::value(&queries_), "add query \"key=value\"")
      ("help,h", boost::program_options::bool_switch(&help_), "show this message")
      ("chunked,C", boost::program_options::bool_switch(&chunked_), "chunked transfer encoding for request body")
      ("header,H", boost::program_options::value(&headers_), "add header \"key:value\"")
      ("basic,b", boost::program_options::value(&basic_), "basic auth using login:password")
      ("close,x", boost::program_options::bool_switch(&close_), "use 'Connection:close' (keep-alive by default)")
      ("method,m", boost::program_options::value(&method_), "request method (GET by default)")
      ("upload,f", boost::program_options::value(&upload_), "file to upload (using PUT by default)")
      ("gzip,z", boost::program_options::bool_switch(&gzip_), "gzip message body")
      ("accept-gz,Z", boost::program_options::bool_switch(&accept_gzip_), "add \"Accept-Encoding: gzip\"")
      ("number,n", boost::program_options::value(&num_requests_)->default_value(1), "number of requests (1 by default)")
      ("connections,c", boost::program_options::value(&num_conns_), "number of connections (1 by default)");

    boost::program_options::store(boost::program_options::command_line_parser(argc, argv).
              options(options_).positional(url_option).run(), vm_);
    boost::program_options::notify(vm_);
  }

  void run()
  {
    if (help_ || url_.empty()) {
      std::cout << "Hello http[s] client world!" << std::endl;
      std::cout << "Usage: http-toy [options] http[s]://hostname[:port][/path]" << std::endl;
      std::cout << options_ << std::endl;
      return;
    }

    URL parsed_url;
    boost::system::error_code ec;
    URL::parse_url(ec, parsed_url, url_);
    if (ec)
    {
      std::cerr << "Bad url" << std::endl;
      return;
    }
    for (queries_type::const_iterator it = queries_.begin(), end = queries_.end(); it != end; ++it)
    {
      parsed_url.add_query(ec, *it);
      if (ec)
      {
        std::cerr << "Bad query: \"" << *it << "\"" << std::endl;
        return;
      }
    }

    const std::string& method = get_method();
    boost::shared_ptr<HTTPRequest> request( boost::make_shared<HTTPRequest>(parsed_url, method) );

    if (!basic_.empty())
    {
      request->add_basic_auth(basic_);
    }

    if (close_)
    {
      request->add_header("Connection", "close");
    }

    if (chunked_)
    {
      request->add_header("Transfer-Encoding", "chunked");
    }

    if (accept_gzip_)
    {
      request->add_header("Accept-Encoding", "gzip");
    }

    for (headers_type::const_iterator it = headers_.begin(), end = headers_.end(); it != end; ++it)
    {
      request->add_header(ec, *it);
      if (ec)
      {
        std::cerr << "Bad header: \"" << *it << "\"" << std::endl;
        return;
      }
    }

    if (gzip_)
    {
      // TODO
      // request-> ???
    }

    boost::asio::io_service io_svc;
    HTTPClient client(io_svc);

    for (int r = 0; r < num_requests_; ++r)
    {
      client.send_request(request,
        boost::bind(&ClientOptions::request_arrived, this, _1, _2, request, boost::ref(client)));
    }

    io_svc.run();
  }

private:

  boost::program_options::options_description options_;
  boost::program_options::variables_map vm_;

  const std::string& get_method() const
  {
    if (!method_.empty())
    {
      return method_;
    }

    if (!upload_.empty())
    {
      static std::string put("PUT");
      return put;
    }

    // All requests are GET by default
    static std::string get("GET");
    return get;
  }

  void request_arrived(const boost::system::error_code& error,
      boost::shared_ptr<HTTPResponse> response,
      boost::shared_ptr<HTTPRequest> request,
      HTTPClient& client)
  {
    std::cout << __FUNCTION__ << " "
        << (!error ? "successfully" :
                     (std::string("with error(") + error.message() + ")").c_str())
        << std::endl;

    if (response)
    {

      std::string body(
          response->get_content().begin(),
          response->get_content().end());
      std::cout << "||" << body << "||" << std::endl;
      /*
      if (response->is_gzipped())
      {
        std::cout << "it was gzipped" << std::endl;
      }
      */
    }

    if (!error)
    {
      schedule_more(client);
    }
  }

  void schedule_more(HTTPClient& client)
  {
    static int acc = 0;
    static int counter = 300000;
    if (!counter)
    {
      return;
    }
    --counter;

    ++acc;



    static URL url("http", "localhost", 8081, "/hello/world");
    static boost::shared_ptr<HTTPRequest> request;

    if (!request)
    {
      request = boost::make_shared<HTTPRequest>(url, "GET");
      request->add_header("Connection", "Keep-Alive");
    }
    if (acc > 100)
    {
      for (int i = 0; i < 100; ++i)
        client.send_request(request, boost::bind(&ClientOptions::request_arrived, this, _1, _2, request, boost::ref(client)));
      acc = 0;
    }
  }

  std::string url_;
  typedef std::vector<std::string> queries_type;
  queries_type queries_;
  bool help_;
  bool chunked_;
  typedef std::vector<std::string> headers_type;
  headers_type headers_;
  std::string basic_;
  bool close_;
  std::string method_;
  std::string upload_;
  bool gzip_;
  bool accept_gzip_;
  int num_requests_;
  int num_conns_;
};

#endif // HTTP_TOY_OPTIONS_HPP
