/*
 * (C) Copyright 2015 Konstantin Chigintsev (kchigintsev@gmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 */

#ifndef HTTP_TOY_HTTP_RESPONSE_HPP
#define HTTP_TOY_HTTP_RESPONSE_HPP

#include <istream>
#include <map>
#include <string>
#include <boost/asio.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/shared_ptr.hpp>

class HTTPResponse : public DataBlock
{
public:
  HTTPResponse()
      : code_(200)
      , status_("OK")
      , ungzipped_(false)
  {
    http_version_ = "HTTP/1.1";
  }

  void set_code(std::size_t code)
  {
    code_ = code;
  }

  std::size_t get_code() const
  {
    return code_;
  }

  void set_status(const std::string& status)
  {
    status_ = status;
  }

  const std::string& get_status() const
  {
    return status_;
  }

  bool is_gzipped() const
  {
    const std::pair<bool, std::string>& encoding = get_header("Content-Encoding");
    return encoding.first && encoding.second.find("gzip") != std::string::npos;
  }

  void ungzip()
  {
    if (!is_gzipped() || ungzipped_)
    {
      return;
    }
    std::vector<char> decompressed;
    boost::iostreams::filtering_ostream os;
    os.push(boost::iostreams::gzip_decompressor());
    os.push(boost::iostreams::back_inserter(decompressed));
    boost::iostreams::write(os, &content_[0], content_.size());
    os.flush();
    decompressed.swap(content_);
    ungzipped_ = true;
  }

  void to_data_stream(content_buf_type& response_buf)
  {
    response_buf.clear();

    if (!get_header("Content-Length").first)
    {
      add_header("Content-Length", boost::lexical_cast<std::string>(get_content().size()));
    }

    std::stringstream response;
    response << http_version_ << " " << code_ << " " << status_ << "\r\n";
    for (headers_map::const_iterator it = headers_.begin(), end =
        headers_.end(); it != end; ++it)
    {
      response << it->first << ": " << it->second << "\r\n";
    }
    response << "\r\n";


    response.seekg(0, std::ios::end);
    std::size_t size = response.tellg();
    response.seekg(0, std::ios::beg);

    // back_inserter looked better but it was a bottleneck
    response_buf.resize(size + content_.size());
    std::copy(std::istreambuf_iterator<char>(response), std::istreambuf_iterator<char>(), response_buf.begin());
    std::copy(content_.begin(), content_.end(), response_buf.begin() + size);

    return;
  }

  static boost::shared_ptr<HTTPResponse> parse_header(
      boost::asio::streambuf& response_buf)
  {
    //std::cout << __FUNCTION__ << std::endl;
    boost::shared_ptr<HTTPResponse> response( boost::make_shared<HTTPResponse>());
    std::istream iss(&response_buf);

    iss >> response->http_version_;
    iss >> response->code_;
    std::getline(iss, response->status_);

//    std::cout << response->http_version_ << " " << response->code_
//        << response->status_ << std::endl;

    std::string header;
    while (std::getline(iss, header) && header != "\r")
    {
      std::size_t separator = header.find(':');
      if (separator == std::string::npos)
      {
        return boost::shared_ptr<HTTPResponse>();
      }

      std::string key(header.begin(), header.begin() + separator);
      std::string value(header.begin() + separator + 2, header.end() - 1);
      response->headers_.insert(std::make_pair(key, value));
//      std::cout << key << ":" << value << std::endl;
    }

    if (response->is_chunked())
    {
      return response;
    }
    else
    {
      boost::asio::streambuf::const_buffers_type content_buf =
              response_buf.data();
      response->content_.assign(boost::asio::buffers_begin(content_buf),
          boost::asio::buffers_end(content_buf));
//      std::cout << "BUFFERED CONTENT: " << std::endl
//          << std::string(response->content_.begin(), response->content_.end())
//          << std::endl << "CONTENT END" << std::endl;
    }

    return response;
  }

private:

  std::size_t code_;
  std::string status_;
  bool ungzipped_;
};

#endif // HTTP_TOY_HTTP_RESPONSE_HPP
