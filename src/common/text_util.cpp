/*
 * (C) Copyright 2015 Konstantin Chigintsev (kchigintsev@gmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 */

#include <boost/algorithm/string/find.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/archive/iterators/base64_from_binary.hpp>
#include <boost/archive/iterators/transform_width.hpp>
#include <boost/archive/iterators/ostream_iterator.hpp>

#include "text_util.hpp"

namespace text
{

std::string encode_base64(const std::string& data)
{
  typedef boost::archive::iterators::base64_from_binary<
      boost::archive::iterators::transform_width<const char *, 6, 8> > base64_text;

  std::string encoded;
  std::copy(base64_text(data.c_str()),
      base64_text(data.c_str() + data.size()), std::back_inserter(encoded));
  return encoded;
}

std::string encode_url_path(const std::string& data)
{
  static std::set<char> slash;
  if (slash.empty())
  {
    slash.insert('/');
  }
  return encode_url(data, slash);
}

std::string encode_url_query(const std::string& data)
{
  static std::set<char> nothing;
  return encode_url(data, nothing);
}

std::string encode_url(const std::string& data, const std::set<char>& allowed)
{
  static std::set<char> always_safe;
  if (always_safe.empty())
  {
    const char safe[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_.-";
    std::copy(safe, safe+sizeof(safe)/sizeof(char), std::inserter(always_safe, always_safe.end()));
  }

  std::string encoded;
  encoded.reserve(data.size());
  for (std::string::const_iterator it = data.begin(), end = data.end(); it != end; it++)
  {
    if ((always_safe.find(*it) != always_safe.end()) || (allowed.find(*it) != allowed.end()))
    {
      encoded += *it;
    }
    else
    {
      const char* hex = "0123456789ABCDEF\0";
      encoded += "%";
      encoded += hex[((*it) >> 4) & 0x0F];
      encoded += hex[(*it) & 0x0F];
    }
  }
  return encoded;
}


// case insensitive search check
bool ifind(const std::string& source, const std::string& target)
{
  return boost::algorithm::ifind_first(source, target).begin() != source.end();
}

// case insensitive comparator
bool icase_less::operator()(const std::string &lhs, const std::string &rhs) const
{
  // Boost version is likely to be slower
#if defined(_WIN32) || defined(_WIN64)
  return _stricmp(lhs.c_str(), rhs.c_str()) < 0;
#else
  return strcasecmp(lhs.c_str(), rhs.c_str()) < 0;
#endif
}


} // namespace text

