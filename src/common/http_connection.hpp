/*
 * (C) Copyright 2015 Konstantin Chigintsev (kchigintsev@gmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 */

#ifndef HTTP_TOY_HTTP_CONNECTION_HPP
#define HTTP_TOY_HTTP_CONNECTION_HPP

#include <iostream>
#include <string>

#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <boost/make_shared.hpp>
#include <boost/optional.hpp>
#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>

#include "error_code.hpp"


class HTTPConnection
{
public:

  virtual ~HTTPConnection()  {}

  typedef boost::function<void(const boost::system::error_code&, std::size_t)> ReadWriteHandler;

  virtual void async_write(const std::vector<char>& data,
      ReadWriteHandler handler) = 0;
  virtual void async_read(std::vector<char>& data,
      ReadWriteHandler handler) = 0;
  virtual void async_read(boost::asio::streambuf& data, std::size_t chunk_size,
        ReadWriteHandler handler) = 0;
  virtual void async_read_until(boost::asio::streambuf& data,
      const std::string& token, ReadWriteHandler handler) = 0;
  virtual void async_read_until_eof(std::vector<char>& data,
      ReadWriteHandler handler) = 0;
};


class HTTPConnector
{
private:
  typedef boost::asio::ip::tcp::socket tcp_socket;
  typedef boost::asio::ssl::stream<tcp_socket> ssl_socket;

  typedef boost::asio::ip::tcp::acceptor acceptor;

public:

  HTTPConnector(boost::asio::io_service& io_svc)
      : io_svc_(io_svc)
      , context_(boost::asio::ssl::context::sslv23)
      , server_context_(boost::asio::ssl::context::tlsv1_server)
  {
    //context_.load_verify_file("ca.pem");
    context_.set_options(boost::asio::ssl::context::default_workarounds |
        boost::asio::ssl::context::no_sslv2 | boost::asio::ssl::context::sslv3 |
        boost::asio::ssl::context::tlsv11 | boost::asio::ssl::context::tlsv12);

    server_context_.set_options(boost::asio::ssl::context::default_workarounds |
        boost::asio::ssl::context::no_sslv2 | boost::asio::ssl::context::sslv3
        | boost::asio::ssl::context::tlsv11 | boost::asio::ssl::context::tlsv12
        | boost::asio::ssl::context::single_dh_use);
    server_context_.use_certificate_chain_file("server.pem");
    server_context_.use_private_key_file("server.pem", boost::asio::ssl::context::pem);
    server_context_.use_tmp_dh_file("dh512.pem");
  }

  typedef boost::shared_ptr<HTTPConnection> connection_ptr;
  typedef boost::function<void(const boost::system::error_code&, connection_ptr)> ConnectHandler;

  void async_connect(boost::asio::ip::tcp::resolver::iterator iterator,
      const std::string& protocol, ConnectHandler handler)
  {
    if (protocol == "https")
    {
      boost::shared_ptr<ssl_socket> socket( produce_ssl_socket() );
      socket->set_verify_mode(boost::asio::ssl::context::verify_none);

      boost::asio::async_connect(socket->lowest_layer(), iterator,
          boost::bind(&HTTPConnector::on_socket_pre_connected<ssl_socket>, this,
              boost::asio::placeholders::error, socket, handler));
    }
    else if (protocol == "http")
    {
      boost::shared_ptr<tcp_socket> socket( produce_socket() );

      boost::asio::async_connect(socket->lowest_layer(), iterator,
          boost::bind(&HTTPConnector::on_socket_connected<tcp_socket>, this,
              boost::asio::placeholders::error, socket, handler));
    }
    else
    {
      handler(http_client::unsupported_protocol, connection_ptr());
    }
  }

  template<typename AcceptorType, typename SocketType, typename ServerContext>
  struct AcceptorContext
  {
    typedef boost::function<boost::shared_ptr<SocketType> ()> socket_producer_type;
    typedef boost::function<void(const boost::shared_ptr<AcceptorContext>& ) > async_accept_type;

    AcceptorContext(boost::asio::io_service& io_svc,
        boost::asio::ip::tcp::endpoint endpoint,
        const boost::shared_ptr<ServerContext>& srv_ctx,
        socket_producer_type sock_producer,
        async_accept_type accept_impl)
    : io_svc_(io_svc)
    , acceptor_(io_svc)
    , endpoint_(endpoint)
    , srv_ctx_(srv_ctx)
    , sock_producer_(sock_producer)
    , async_accept_impl(accept_impl)
    {
      acceptor_.open(endpoint_.protocol());
      acceptor_.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
      acceptor_.bind(endpoint_);
      acceptor_.listen();
    }

    boost::asio::io_service& io_svc_;
    AcceptorType acceptor_;

    boost::asio::ip::tcp::endpoint endpoint_;

    boost::shared_ptr<ServerContext> srv_ctx_;
    socket_producer_type sock_producer_;
    boost::shared_ptr<SocketType> socket_;

    async_accept_type async_accept_impl;
  };

  template<typename ServerContext>
  void async_accept(const std::string& protocol, unsigned int port, const boost::shared_ptr<ServerContext>& srv_ctx)
  {
    if (protocol == "https")
    {
      typedef AcceptorContext<acceptor, ssl_socket, ServerContext> acceptor_context_type;
      boost::shared_ptr<acceptor_context_type> acceptor_ctx( new acceptor_context_type(
          io_svc_,
          boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port),
          srv_ctx,
          boost::bind(&HTTPConnector::produce_ssl_socket, this, true),
          boost::bind(&HTTPConnector::accept_ssl_impl<acceptor, ssl_socket, ServerContext>, this, _1)
          ));
      acceptor_ctx->async_accept_impl(acceptor_ctx);
    }
    else if (protocol == "http")
    {
      typedef AcceptorContext<acceptor, tcp_socket, ServerContext> acceptor_context_type;
      boost::shared_ptr<acceptor_context_type> acceptor_ctx( new acceptor_context_type(
          io_svc_,
          boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port),
          srv_ctx,
          boost::bind(&HTTPConnector::produce_socket, this),
          boost::bind(&HTTPConnector::accept_impl<acceptor, tcp_socket, ServerContext>, this, _1)
          ));
      acceptor_ctx->async_accept_impl(acceptor_ctx);
    }
    else
    {
      srv_ctx->on_accepted(http_client::unsupported_protocol, connection_ptr(), srv_ctx);
    }
  }

private:

  template<typename SocketType>
  void on_socket_connected(const boost::system::error_code& error,
      const boost::shared_ptr<SocketType>& socket, ConnectHandler handler);

  template<typename SocketType>
  void on_socket_pre_connected(const boost::system::error_code& error,
      const boost::shared_ptr<SocketType>& socket, ConnectHandler handler)
  {
    if (error)
    {
      handler(error, connection_ptr());
      return;
    }
    socket->async_handshake(boost::asio::ssl::stream_base::client,
        boost::bind(&HTTPConnector::on_socket_connected<SocketType>, this,
            boost::asio::placeholders::error, socket, handler));
  }

  boost::shared_ptr<tcp_socket> produce_socket()
  {
    return boost::shared_ptr<tcp_socket>( new tcp_socket(io_svc_) );
  }

  boost::shared_ptr<ssl_socket> produce_ssl_socket(bool server = false)
  {
    return boost::shared_ptr<ssl_socket>( new ssl_socket(io_svc_, (server ? server_context_ : context_)) );
  }

  template<typename AcceptorType, typename SocketType, typename ServerContext>
  void accept_impl(const boost::shared_ptr<AcceptorContext<AcceptorType, SocketType, ServerContext> >& acc_ctx)
  {
//    std::cout << __FUNCTION__ << std::endl;
    acc_ctx->socket_ = acc_ctx->sock_producer_();
    acc_ctx->acceptor_.async_accept(acc_ctx->socket_->lowest_layer(),
        boost::bind(&HTTPConnector::on_accepted<AcceptorType, SocketType, ServerContext>, this,
            boost::asio::placeholders::error, acc_ctx));
  }

  template<typename AcceptorType, typename SocketType, typename ServerContext>
    void accept_ssl_impl(const boost::shared_ptr<AcceptorContext<AcceptorType, SocketType, ServerContext> >& acc_ctx)
  {
//    std::cout << __FUNCTION__ << std::endl;
    acc_ctx->socket_ = acc_ctx->sock_producer_();
    acc_ctx->socket_->set_verify_mode(boost::asio::ssl::context::verify_none);
    acc_ctx->acceptor_.async_accept(acc_ctx->socket_->lowest_layer(),
        boost::bind(&HTTPConnector::on_pre_accepted<AcceptorType, SocketType, ServerContext>, this,
            boost::asio::placeholders::error, acc_ctx));
  }

  template<typename AcceptorType, typename SocketType, typename ServerContext>
  void on_pre_accepted(const boost::system::error_code& error,
      const boost::shared_ptr<AcceptorContext<AcceptorType, SocketType, ServerContext> >& acc_ctx)
  {
//    std::cout << __FUNCTION__ << " "
//            << (!error ? "successfully" :
//                         (std::string("with error(") + error.message() + ")").c_str())
//            << std::endl;
    if (error)
    {
      acc_ctx->srv_ctx_->on_accepted(error, HTTPConnector::connection_ptr(), acc_ctx->srv_ctx_);
      return;
    }
    acc_ctx->socket_->async_handshake(boost::asio::ssl::stream_base::server,
        boost::bind(&HTTPConnector::on_accepted<AcceptorType, SocketType, ServerContext>, this,
            boost::asio::placeholders::error, acc_ctx));
  }

  template<typename AcceptorType, typename SocketType, typename ServerContext>
  void on_accepted(const boost::system::error_code& error,
      const boost::shared_ptr<AcceptorContext<AcceptorType, SocketType, ServerContext> >& acc_ctx);

  boost::asio::io_service& io_svc_;
  boost::asio::ssl::context context_;
  boost::asio::ssl::context server_context_;
}; // class HTTPConnector


template<typename SocketType>
class HTTPConnectionImpl : public HTTPConnection
{
public:
  HTTPConnectionImpl(const boost::shared_ptr<SocketType>& socket)
      : socket_(socket)
  {
  }

  virtual void async_write(const std::vector<char>& data,
      ReadWriteHandler handler)
  {
    boost::asio::async_write(*socket_,
        boost::asio::buffer(&data[0], data.size()),
        boost::bind(handler, boost::asio::placeholders::error,
            boost::asio::placeholders::bytes_transferred));
  }

  virtual void async_read(std::vector<char>& data, ReadWriteHandler handler)
  {
    boost::asio::async_read(*socket_,
        boost::asio::buffer(data),
        boost::bind(handler, boost::asio::placeholders::error,
            boost::asio::placeholders::bytes_transferred));
  }

  virtual void async_read(boost::asio::streambuf& data,
      std::size_t chunk_size, ReadWriteHandler handler)
  {
    boost::asio::async_read(*socket_,
        data, boost::asio::transfer_at_least(chunk_size),
        boost::bind(handler, boost::asio::placeholders::error,
            boost::asio::placeholders::bytes_transferred));
  }

  virtual void async_read_until(boost::asio::streambuf& data,
      const std::string& token, ReadWriteHandler handler)
  {
    boost::asio::async_read_until(*socket_, data, token,
        boost::bind(handler, boost::asio::placeholders::error,
            boost::asio::placeholders::bytes_transferred));
  }

  virtual void async_read_until_eof(std::vector<char>& data,
      ReadWriteHandler handler)
  {
    boost::shared_ptr<boost::asio::streambuf> buf(boost::make_shared<boost::asio::streambuf>());

    boost::asio::async_read(*socket_, *buf,
        boost::bind(&HTTPConnectionImpl<SocketType>::async_read_handler, this,
            boost::asio::placeholders::error,
            boost::asio::placeholders::bytes_transferred, buf, boost::ref(data),
            handler));
  }

private:
  void async_read_handler(const boost::system::error_code& error,
      std::size_t bytes, const boost::shared_ptr<boost::asio::streambuf>& buf,
      std::vector<char>& data, ReadWriteHandler handler)
  {
    boost::asio::streambuf::const_buffers_type content_buf = buf->data();
    data.assign(boost::asio::buffers_begin(content_buf),
        boost::asio::buffers_end(content_buf));

    handler(
        (error == boost::asio::error::eof ? boost::system::error_code() : error),
        bytes);
  }
  boost::shared_ptr<SocketType> socket_;
}; // class HTTPConnectionImpl


template<typename SocketType>
void HTTPConnector::on_socket_connected(const boost::system::error_code& error,
    const boost::shared_ptr<SocketType>& socket, ConnectHandler handler)
{
  handler(error,
      boost::shared_ptr<HTTPConnection>(
          boost::make_shared<HTTPConnectionImpl<SocketType> >(socket)));
}

template<typename AcceptorType, typename SocketType, typename ServerContext>
void HTTPConnector::on_accepted(const boost::system::error_code& error,
      const boost::shared_ptr<AcceptorContext<AcceptorType, SocketType, ServerContext> >& acc_ctx)
{
//  std::cout << __FUNCTION__ << " "
//          << (!error ? "successfully" :
//                       (std::string("with error(") + error.message() + ")").c_str())
//          << std::endl;
  acc_ctx->srv_ctx_->on_accepted( error,
      (acc_ctx->socket_
          ? HTTPConnector::connection_ptr(new HTTPConnectionImpl<SocketType>(acc_ctx->socket_))
          : HTTPConnector::connection_ptr()), acc_ctx->srv_ctx_ );

  acc_ctx->async_accept_impl(acc_ctx);
}

#endif // HTTP_TOY_HTTP_CONNECTION_HPP
