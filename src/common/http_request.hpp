/*
 * (C) Copyright 2015 Konstantin Chigintsev (kchigintsev@gmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 */

#ifndef HTTP_TOY_HTTP_REQUEST_HPP
#define HTTP_TOY_HTTP_REQUEST_HPP

#include <map>
#include <vector>
#include <string>
#include <sstream>
#include <utility>

#include <boost/make_shared.hpp>

#include "data_block.hpp"
#include "text_util.hpp"
#include "url.hpp"

class HTTPRequest : public DataBlock
{

public:
  HTTPRequest(const URL& url, const std::string& method)
  : method_(method)
  {
    http_version_ = "HTTP/1.1";
    redirect(url);
  }

  const std::string& get_method() const
  {
    return method_;
  }

  void add_basic_auth(const std::string& username,
      const std::string& password)
  {
    std::cout << __FUNCTION__ << std::endl;
    add_basic_auth(username + ":" + password);

  }

  void add_basic_auth(const std::string& login)
  {
    std::cout << __FUNCTION__ << std::endl;
    add_header("Authorization", "Basic " + text::encode_base64(login));
  }

  const URL& get_url() const
  {
    return url_chain_.back();
  }

  void redirect(const URL& new_url)
  {
    url_chain_.push_back(new_url);
  }

  boost::shared_ptr<content_buf_type> to_data_stream()
  {
    if (!get_header("Content-Length").first)
    {
      add_header("Content-Length", boost::lexical_cast<std::string>(get_content().size()));
    }

    std::stringstream request;
    //request << get_method() << " " << get_url().get_path() << " HTTP/1.1\r\n";
    request << get_method() << " " << get_url().get_path() << get_url().get_query() << " " << http_version_ << "\r\n";
    request << "Host: " << get_url().get_host() << "\r\n";
    for (headers_map::const_iterator it = headers_.begin(), end =
        headers_.end(); it != end; ++it)
    {
      request << it->first << ": " << it->second << "\r\n";
    }
    request << "\r\n";

    //const std::string& request_str = request.str();
    //std::cout << request_str << std::endl;

    boost::shared_ptr<content_buf_type> request_buf(
        boost::make_shared<content_buf_type>(std::istreambuf_iterator<char>(request), std::istreambuf_iterator<char>()) );
    std::copy(content_.begin(), content_.end(), std::back_inserter(*request_buf));
    return request_buf;
  }

  static boost::shared_ptr<HTTPRequest> parse_header(
        boost::asio::streambuf& request_buf)
  {
//    std::cout << __FUNCTION__ << std::endl;
    boost::shared_ptr<HTTPRequest> request(boost::make_shared<HTTPRequest>());
    std::istream iss(&request_buf);

    iss >> request->method_;

    std::string path;
    iss >> path;

    iss >> request->http_version_;
    std::getline(iss, request->http_version_);
    request->http_version_.resize(std::max<std::size_t>(request->http_version_.size()-1, 0));

//    std::cout << request->method_ << " " << path << " " << request->http_version_ <<  std::endl;

    boost::system::error_code ec;
    DataBlock::parse_header(ec, static_cast<DataBlock&>(*request), request_buf);
    if (ec)
    {
      return boost::shared_ptr<HTTPRequest>();
    }

    std::string host = request->get_header("Host").second;
    URL incoming_url;
    URL::parse_url(ec, incoming_url, host + path);
    if (!ec)
    {
      request->redirect(incoming_url);
    }

    return request;
  }

  // private ctor for parsing purposes.
  HTTPRequest()
  {}

private:

  //typedef std::vector<char> buffer_type;



  std::string method_;
  std::vector<URL> url_chain_;
};

#endif // HTTP_TOY_HTTP_REQUEST_HPP
