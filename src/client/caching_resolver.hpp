/*
 * (C) Copyright 2015 Konstantin Chigintsev (kchigintsev@gmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 */

#ifndef HTTP_TOY_CACHING_RESOLVER_HPP
#define HTTP_TOY_CACHING_RESOLVER_HPP

#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/placeholders.hpp>
#include <boost/function.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/shared_ptr.hpp>

class CachingResolver
{
public:
  CachingResolver(boost::asio::io_service& io_svc)
      : io_svc_(io_svc),
        resolver_(io_svc)
  {
  }

  typedef boost::function<
      void(const boost::system::error_code&,
          boost::asio::ip::tcp::resolver::iterator)> Handler;

  void async_resolve(const std::string& host, unsigned int port,
      Handler handler)
  {
//    std::cout << __FUNCTION__ << std::endl;
    io_svc_.post(
        boost::bind(&CachingResolver::async_resolve_impl, this, host, port,
            handler));
  }

private:

  typedef std::pair<std::string, unsigned int> cache_key_type;

  void async_resolve_impl(const std::string& host, unsigned int port,
      Handler handler)
  {
//    std::cout << __FUNCTION__ << std::endl;
    cache_key_type key( std::make_pair(host, port) );
    endpoints_cache_type::iterator resolved = endpoints_cache_.find(key);
    if (resolved != endpoints_cache_.end())
    {
//      std::cout << "getting from cache: " << host << ":" << port << std::endl;
//      std::cout << "resolution_handler" << std::endl;
      handler(boost::system::error_code(), (*resolved).second);
      return;
    }

    bool resolution_needed = (handlers_cache_.find(key) == handlers_cache_.end());
    handlers_cache_.insert(std::make_pair(key, handler));

    if (resolution_needed)
    {
      boost::asio::ip::tcp::resolver::query query(host,
          boost::lexical_cast<std::string>(port));
      resolver_.async_resolve(query,
          boost::bind(&CachingResolver::on_resolve, this,
              boost::asio::placeholders::error,
              boost::asio::placeholders::iterator, key));
    }
  }

  void on_resolve(const boost::system::error_code& error,
      boost::asio::ip::tcp::resolver::iterator endpoint_iterator,
      const cache_key_type& key)
  {
//    std::cout << __FUNCTION__ << " "
//        << (!error ?
//            "successfully" :
//            (std::string("with error(") + error.message() + ")").c_str())
//        << std::endl;
    if (!error)
    {
//      std::cout << "saving to cache: " << key.first << ":" << key.second << std::endl;
      endpoints_cache_[key] = endpoint_iterator;
    }
    std::pair<handlers_cache_type::iterator, handlers_cache_type::iterator> it =
        handlers_cache_.equal_range(key);
    for (; it.first != it.second ; )
    {
//      std::cout << "resolution_handler" << std::endl;
      Handler& handler = it.first->second;
      handler(error, endpoint_iterator);
      handlers_cache_.erase(it.first++);
    }
  }

  boost::asio::io_service& io_svc_;
  boost::asio::ip::tcp::resolver resolver_;

  typedef std::map<cache_key_type, boost::asio::ip::tcp::resolver::iterator> endpoints_cache_type;
  endpoints_cache_type endpoints_cache_;
  typedef std::multimap<cache_key_type, Handler> handlers_cache_type;
  handlers_cache_type handlers_cache_;
};

#endif // HTTP_TOY_CACHING_RESOLVER_HPP
