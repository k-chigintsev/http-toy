/*
 * (C) Copyright 2015 Konstantin Chigintsev (kchigintsev@gmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 */

#include <iostream>

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>

#include "http_server.hpp"
#include "http_request.hpp"
#include "http_response.hpp"

#include "fs_static_server.hpp"

namespace
{

boost::shared_ptr<HTTPResponse> handle_request(
    boost::system::error_code& error,
    //boost::shared_ptr<HTTPRequest> request,
    HTTPRequest& request,
    boost::asio::io_service& io_svc)
{
  static boost::shared_ptr<HTTPResponse> not_found;
  if (!not_found)
  {
    not_found.reset(new HTTPResponse());
    not_found->set_code(200);
    not_found->set_status("OK");
    std::string msg("<title>HTTP-Toy Server</title><body>");
    for (int i = 0; i<15; ++i)
    {
      msg += "<h1>There should be some text</h1><br/>";
    }
//    for (int i = 0; i<3300000; ++i)
//    {
//      msg += "<h1>Nothing to see here</h1><br/>";
//    }
    msg += "</body>\r\n";
    not_found->add_content(msg.begin(), msg.end());
  }

  error = boost::system::error_code();
  //error = http_client::header_parsing_error;

  static std::size_t counter = 0;
  if (++counter == 10000000000)
  {
    io_svc.stop();
  }

  return not_found;
}

}

int main(int argc, char* argv[])
{
  try
  {
    boost::asio::io_service io_svc;
    HTTPServer server(io_svc);
    FSStaticServer static_server(io_svc, "/home/chigkon/hobby/boost_1_58_0/");
    std::cout << "hello https server side world" << std::endl;

    server.start_server("http", 8080, boost::bind(&FSStaticServer::process_request, &static_server, _1, _2));
    server.start_server("http", 8081, boost::bind(&FSStaticServer::process_request, &static_server, _1, _2));
    server.start_server("http", 8082, boost::bind(&FSStaticServer::process_request, &static_server, _1, _2));
    server.start_server("http", 8083, boost::bind(handle_request, _1, _2, boost::ref(io_svc)));
    server.start_server("http", 8084, boost::bind(handle_request, _1, _2, boost::ref(io_svc)));
    server.start_server("https", 8443, boost::bind(&FSStaticServer::process_request, &static_server, _1, _2));
    server.start_server("https", 8444, boost::bind(&FSStaticServer::process_request, &static_server, _1, _2));
    server.start_server("https", 8445, boost::bind(&FSStaticServer::process_request, &static_server, _1, _2));
    server.start_server("https", 8446, boost::bind(handle_request, _1, _2, boost::ref(io_svc)));
    server.start_server("https", 8447, boost::bind(handle_request, _1, _2, boost::ref(io_svc)));

//    boost::shared_ptr<boost::asio::io_service::work> work( new boost::asio::io_service::work(io_svc) );
//    boost::thread worker( boost::bind(&boost::asio::io_service::run, &io_svc ) );

    io_svc.run(); // no way out from here! it will work forever :-p
//    work.reset();
//    worker.join();

    std::cout << "done!" << std::endl;
  }
  catch (std::exception& ex)
  {
    std::cerr << "Error occurred: " << ex.what() << std::endl;
  }
  return 0;
}
