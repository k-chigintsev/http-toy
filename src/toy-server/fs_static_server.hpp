/*
 * (C) Copyright 2015 Konstantin Chigintsev (kchigintsev@gmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 */

#ifndef HTTP_TOY_FS_STATIC_SERVER_HPP
#define HTTP_TOY_FS_STATIC_SERVER_HPP

#include <string>
#include <vector>

#include <boost/asio.hpp>
#include <boost/filesystem.hpp>
#include <boost/function.hpp>
#include <boost/iostreams/device/mapped_file.hpp>
#include <boost/shared_ptr.hpp>

#include "http_connection.hpp"
#include "predefined_response.hpp"

class HTTPRequest;
class HTTPResponse;

class FSStaticServer
{
public:

  typedef boost::shared_ptr<HTTPConnection> ConnectionPtr;
  typedef boost::shared_ptr<HTTPRequest> RequestPtr;
  typedef boost::shared_ptr<HTTPResponse> ResponsePtr;

  FSStaticServer(boost::asio::io_service& io_svc, const boost::filesystem::path& root)
  : io_svc_(io_svc)
  , root_(root)
  {
    if (!boost::filesystem::exists(root_))
    {
      throw std::exception();
    }
  }

  //typedef boost::function<ResponsePtr (boost::system::error_code&, HTTPRequest&)> RequestProcessor;
  ResponsePtr process_request(boost::system::error_code& ec, HTTPRequest& request)
  {
    ec = boost::system::error_code();
    ResponsePtr result;
    try
    {
      if (request.get_method() != "GET")
      {
        return PredefinedResponse::forbidden();
      }

      std::string path = request.get_url().get_path();
      cache_type::iterator it = cache_.find(path);
      if (it != cache_.end())
      {
        std::cout << "GET fetch cached response for url " << path << std::endl;
        return it->second;
      }

      result = boost::make_shared<HTTPResponse>();
      fill_in_response(request, path, *result);

      cache_[path] = result;
    }
    catch (std::exception&)
    {
      result = PredefinedResponse::internal_error();
    }
    return result;
  }

  void fill_in_response(HTTPRequest& request, const std::string& path, HTTPResponse& response)
  {
    // security concerns are not important because this class' goal is to test the network part of http-server
    // it is not a production quality static content web-server.
    boost::filesystem::path target( root_ / path );
    std::cout << "GET request url: " << path << " -> " << target << std::endl;

    if (boost::filesystem::exists(target))
    {
      // actually there are more fs object types but lets brush over it for simplicity
      std::cout << path << " exists and it is a "
          << (boost::filesystem::is_directory(target) ? "directory" : "file") << std::endl;

      if (boost::filesystem::is_regular_file(target))
      {
        boost::uintmax_t size = boost::filesystem::file_size(target);
        std::cout << path << " file size: " << size << std::endl;

        boost::iostreams::mapped_file mf(target);
        if (mf.is_open())
        {
          response.add_content(mf.data(), mf.data() + mf.size());
        }

        response.set_code(200);
        response.set_status("OK");
      }
      else
      {
        response.set_code(200);
        response.set_status("OK");

        std::string msg("<title>HTTP-Toy Server</title><body><h1>Directory listings are not shown</h1></body>\r\n");
        response.add_content(msg.begin(), msg.end());
      }
    }
    else
    {
      std::cout << path << " does not exist" << std::endl;
    }
  }

private:

  boost::asio::io_service& io_svc_;
  boost::filesystem::path root_;
  typedef std::map<std::string, ResponsePtr> cache_type;
  cache_type cache_;
};

#endif // HTTP_TOY_FS_STATIC_SERVER_HPP
