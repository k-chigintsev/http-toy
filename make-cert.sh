#!/bin/bash

# Copied from stackoverflow with minor changes
# http://stackoverflow.com/questions/12217346/c-boost-asio-error-no-shared-cipher/12492750#12492750

# Generate a new ssl private key :
openssl genrsa -out privkey.pem 1024

# Create a certificate signing request using your new key
openssl req -new -key privkey.pem -out certreq.csr

# Self-sign your CSR with your own private key:
openssl x509 -req -days 3650 -in certreq.csr -signkey privkey.pem -out newcert.pem

# Install the signed certificate and private key for use by an ssl server
# This allows you to use a single file for certificate and private key
dd if=<( openssl x509 -in newcert.pem ; cat privkey.pem ) of=server.pem

# If you use a dh temp file :
openssl dhparam -outform PEM -out dh512.pem 512
